console.log("array");

/*Basic Array Structure
  Access Elements in a Array -through index

Two ways to initialize an Array

  */
  let array = [1,2,3];
  console.log(array);

  let arr = new Array(1,2,3);
console.log(arr);
// index = array.length - 1
console.log(array[0]);
console.log(array[1]);
console.log(array[2]);

// Array Manipulation

let count = ["one", "two", "three,","four"];

//Q: How are we going to add a new element at the end of the of an array

console.log(count.length);
console.log(count[4]);

/*Using assignment operator (=)*/
count[4] = "five";
console.log(count);

//Push method Syntax: array.push()
 //add element at the end of an array
count.push("element");
console.log(count);


function pushMethod(element){
	return count.push(element)
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");

console.log(count);

//Pop Method Syntax: array.pop()
// Removes last element of an array
count.pop();
console.log(count);
//Q: Can we pop method to remove "four" element? - no, it will still remove the last element no matter what.
count.pop("four");
console.log(count);

function popMethod(){
   count.pop()
}
popMethod();
console.log(count);


//Q: How do we add element at the beginning of an array?
// unshift method Syntax: array.unshift()
count.unshift("hugot");
console.log(count);

function unshiftMethod(element){
	return count.unshift(element);
}

unshiftMethod("zero");
console.log(count);

/*Can we also remove element at the beginning of an array? - Yes*/
count.shift();
console.log(count);

function shiftMethod(){
	return count.shift();
}
shiftMethod();
console.log(count);


//Sort Method Syntax: array.sort()

let nums = [15,32,61,130,230,13,34];
nums.sort();
console.log(nums);

//sort nums in ascending order

nums.sort(
     function(a,b){
         return b - a 
     }

	)
console.log(nums);

//Reverse method array.reverse()
nums.reverse();
console.log(nums);


/*Splice and Slice*/
// Splice method array.splice()
// returns an array of of ommited elements 
// it directly manipulates the original array
// first parameter - index where to start ommiting element
// second parameter - # number of elements to be ommited starting from first parameter 
// third parameter 	- elements to be added in place of the ommited elements.
console.log(count);
/*
let newSplice = count.splice(1);
console.log(newSplice);
console.log(count);*/

/*let newSplice = count.splice(1,2);
console.log(newSplice);
console.log(count);*/
/*
let newSplice = count.splice(1,2,"a1","b2", "c3");
console.log(count);*/

//Slice Method array.slice(start, end)
// first parameter - index where to begin ommitting elements
// second paramater - #nubmer of elements to be ommited (index - 1)
console.log(count);

/*let newSlice = count.slice(1);
console.log(newSlice);
console.log(count);
*/
let newSlice =count.slice(3,6);
console.log(newSlice);

//concat  array.concat();
// method used to merged two or more arrays.
console.log(count);
console.log(nums);
let animals = ["bird", "cat","dog","fish"];
console.log(animals);
let newConcat = count.concat(nums, animals);
console.log(newConcat)
//join method  array.join(); 
// parameters - "", "-", ","
let meal = ["rice", "steak","juice"];
let newJoin = meal.join()
console.log(newJoin);
 newJoin = meal.join("")
console.log(newJoin);
newJoin = meal.join(" ")
console.log(newJoin);
newJoin = meal.join("-")
console.log(newJoin);

//toString method
console.log(nums)
console.log(typeof nums[3])

let newString = nums.toString();
console.log(newString);

/*Accessors*/
let countries = ["US", "PH", "CAN","PH", "SG", "HK","PH","NZ"];

//IndexOf() array.indexOf()
// finds the index of a given element where it is "first" found.
let index = countries.indexOf("PH");
console.log(index);
//if element is non existing, return is -1
example = countries.indexOf("AU");
console.log(index);

//lastIndexof()
let lastindex = countries.lastIndexOf("PH");
console.log(lastindex);

/*if(countries.indexOf("AU") == -1){
	console.log('Element not existing')
}else{
	console.log('Element exist in the countries array')
}

*/

/*Iterators*/

//forEach()  array.forEach(cb())
//map()      array.map()

let days = ["mon", "tue", "wed", "thu","fri","sat","sun"]
console.log(days);
//forEach
// returns undefined
days.forEach(
     function (element){
       console.log(element);
     }

	)
let mapDays = days.map(function(day){
     return `${day} is the day of the week`
})
console.log(mapDays)
console.log(days)

/*Mini Activity 
using forEach, add each element of days array in an empty array called days2*/
let days2 = days.slice(0,7);
days2.forEach(
	function(element){
	console.log(days2)
}
)

//filter  array.filter(cb())

console.log(nums);

let newFilter = nums.filter(function(num){
   return num < 50
}
)
console.log(newFilter);

// includes array.includes()
// returns boolean value if a word is existing in the array.
console.log(animals);

let newIncludes = animals.includes("bird");
console.log(newIncludes);
/* mini activity 

create a function that will accept a value, that if value is found in the array, return a message <value> is found
if value is not found, return <value> not found.

try to invoke function 3x with different values 

*/
    function miniActivtiy(value){
        if(animals.includes(value) == true){
        	return `${value} is found`
    }else{
    	return `${value} is not found`
    }
}
    console.log(miniActivtiy("cat"));
    console.log(miniActivtiy("ex"));
    console.log(miniActivtiy("fish"));

    //every
       //bolean
       // returns true only if "all elements" passed the given condition
       let newEvery = nums.every(function(num){
       	return ( num > 1)
       })
       console.log(newEvery);

     //some(cb())
        //boleean
       let newSome = nums.some(function(num){
       	return(num > 50)
       })
       console.log(newSome);

      /* let newSome2 = nums.some(num => num > 50)
      console.log(newSome2);*/

      //reduce(cb(<previous>,<current>))

      let newReduce = nums.reduce(function(a,b){
      	  return a + b
      })
      console.log(newReduce);

      //to get the average of nums array 
      // total all the elements 
      // divide it by the total # of elements.
      let average = newReduce/nums.length;

      //toFixed(# of decimals) - returns tring 


      
      console.log(average.toFixed(2));

      //paseInt() or parseFloat()
      console.log(parseFloat(average.toFixed(2)));
